extends Control

signal max_force_changed(value)
signal max_speed_changed(value)
signal boids_number_changed(value)
signal cohesion_distance_changed(value)
signal alignment_distance_changed(value)
signal separation_distance_changed(value)

onready var fps : Label = $FPSContainer/HBox/FPS

onready var force_slider : ExplicitHSlider = $LeftSliders/VBox/ForceSlider
onready var speed_slider : ExplicitHSlider = $LeftSliders/VBox/SpeedSlider
onready var boids_slider : ExplicitHSlider = $LeftSliders/VBox/BoidsSlider

onready var cohesion_slider : ExplicitHSlider = $RightSliders/VBox/CohesionSlider
onready var alignment_slider : ExplicitHSlider = $RightSliders/VBox/AlignmentSlider
onready var separation_slider : ExplicitHSlider = $RightSliders/VBox/SeparationSlider

# warning-ignore:unused_argument
func _process(delta : float) -> void:
    fps.text = str(Engine.get_frames_per_second())

func initiaize(max_force : float, max_speed : float, boids_number : float, maximum_boids_nbr : float, cohesion : float, alignment : float, separation : float) -> void:
# warning-ignore:return_value_discarded
    force_slider.connect('value_changed', self, '_on_ForceSlider_value_changed')
# warning-ignore:return_value_discarded
    speed_slider.connect('value_changed', self, '_on_SpeedSlider_value_changed')
# warning-ignore:return_value_discarded
    boids_slider.connect('value_changed', self, '_on_BoidsSlider_value_changed')
# warning-ignore:return_value_discarded
    cohesion_slider.connect('value_changed', self, '_on_CohesionSlider_value_changed')
# warning-ignore:return_value_discarded
    alignment_slider.connect('value_changed', self, '_on_AlignmentSlider_value_changed')
# warning-ignore:return_value_discarded
    separation_slider.connect('value_changed', self, '_on_SeparationSlider_value_changed')

    force_slider.initialize('Maximum steering force', 0.0, 128.0, max_force, 1.0, true)
    speed_slider.initialize('Maximum speed', 0.0, 1024.0, max_speed, 1.0, true)
    boids_slider.initialize('Boids', 0.0, maximum_boids_nbr, boids_number, 1.0, true)
    cohesion_slider.initialize('Cohesion distance', 0.0, 128.0, cohesion, 1.0, true)
    alignment_slider.initialize('Alignment distance', 0.0, 128.0, alignment, 1.0, true)
    separation_slider.initialize('Separation distance', 0.0, 128.0, separation, 1.0, true)

func _on_ForceSlider_value_changed(value : float) -> void:
    emit_signal('max_force_changed', value)

func _on_SpeedSlider_value_changed(value : float) -> void:
    emit_signal('max_speed_changed', value)

func _on_BoidsSlider_value_changed(value : float) -> void:
    emit_signal('boids_number_changed', value)

func _on_CohesionSlider_value_changed(value : float) -> void:
    emit_signal('cohesion_distance_changed', value)

func _on_AlignmentSlider_value_changed(value : float) -> void:
    emit_signal('alignment_distance_changed', value)

func _on_SeparationSlider_value_changed(value : float) -> void:
    emit_signal('separation_distance_changed', value)

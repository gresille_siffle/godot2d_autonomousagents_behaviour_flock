extends Control
class_name ExplicitHSlider

signal value_changed(value)

onready var displayed_name : Label = $VBox/HBox/Text
onready var displayed_value : Label = $VBox/HBox/Value

onready var hslider : HSlider = $VBox/HSlider

func _ready() -> void:
# warning-ignore:return_value_discarded
    hslider.connect('value_changed', self, '_on_HSlider_value_changed')

func initialize(text : String, min_value : float, max_value : float, value : float, step : float, rounded : bool) -> void:
    displayed_name.text = text
    hslider.min_value = min_value
    hslider.max_value = max_value
    hslider.value = value
    hslider.step = step
    hslider.rounded = rounded
    displayed_value.text = str(value)

func _on_HSlider_value_changed(value : float) -> void:
    displayed_value.text = str(value)
    emit_signal('value_changed', value)

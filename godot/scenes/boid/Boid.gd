extends Sprite
class_name Boid

export (float) var mass = 4.0
export (int) var max_force = 10  # px/s
export (int) var max_speed = 420 # px/s

onready var visibility_notifier : VisibilityNotifier2D = $VisibilityNotifier2D

var velocity : Vector2 = Vector2()
var acceleration : Vector2 = Vector2()

# var steering_force : Vector2 = Vector2()
# var desired_velocity : Vector2 = Vector2()

var slowing_distance : int = 128

var cohesion_distance : int = 64
var alignment_distance : int = 64
var separation_distance : int = 32

func _ready():
# warning-ignore:return_value_discarded
    visibility_notifier.connect('screen_exited', self, '_on_VisibilityNotifier_screen_exited')

func seek(target_position : Vector2) -> Vector2:
    """Return the force that needs to be added to the current velocity to seek the target position.

    Use the `max_force` attribute to clamp the force magnitude.
    """
    # all intermediate steps are commented to speed up the processing
    #
    # var desired_velocity = (target_position - position).normalized() * max_speed
    # var steering_force = (desired_velocity - velocity).clamped(max_force)
    # return steering_force
    return ((target_position - position).normalized() * max_speed - velocity).clamped(max_force)

func seek_with_mass(target_position : Vector2) ->  Vector2:
    """Return the force that needs to be added to the current velocity to seek the target position.

    Use the `mass` attribute to divided the force magnitude.
    """
    # all intermediate steps are commented to speed up the processing
    #
    # var desired_velocity = (target_position - position).normalized() * max_speed
    # var steering_force = ((target_position - position).normalized() * max_speed - velocity) / mass
    # return steering_force
    return ((target_position - position).normalized() * max_speed - velocity) / mass

func seek_and_arrive(target_position : Vector2) -> Vector2:
    """Return the force that needs to be added to the current velocity to seek and slowly approach the target position.

    Use the `max_force` attribute to clamp the force magnitude. Also divide that magnitude when the target position
    is within the the `slowing_distance` range.
    """
    var desired_velocity = (target_position - position)
    var target_distance = desired_velocity.length()

    # some intermediate steps are commented to speed up the processing
    #
    # desired_velocity = desired_velocity.normalized() * max_speed
    # desired_velocity = approach_target(slowing_distance, target_distance, desired_velocity)
    #
    # var steering_force = (desired_velocity - velocity).clamped(max_force)
    # return steering_force
    return (approach_target(
        slowing_distance, target_distance, desired_velocity.normalized() * max_speed
    ) - velocity).clamped(max_force)

# warning-ignore:unused_argument
func approach_target(slowing_distance_ : int, distance_to_target : float, velocity_ : Vector2) -> Vector2:
    if distance_to_target < slowing_distance_:
        velocity_ *= (distance_to_target / slowing_distance_)
    return velocity_

func cohesion(boids : Array) -> Vector2:
    var boid_nbr : int = 0
    var sum_of_position : Vector2 = Vector2()

    for boid_ref in boids:
        var boid : Node2D = boid_ref.get_ref()
        if boid and boid != self:
            var distance : float = position.distance_to((boid as Node2D).position)
            if distance > 0 and distance <= cohesion_distance:
                sum_of_position += boid.position
                boid_nbr += 1

    if boid_nbr:
        return seek(sum_of_position / float(boid_nbr))
    return Vector2()

func alignment(boids : Array) -> Vector2:
    var boid_nbr = 0
    var sum_of_velocity = Vector2()

    for boid_ref in boids:
        var boid = boid_ref.get_ref()
        if boid and boid != self:
            var distance : float = position.distance_to((boid as Node2D).position)
            if distance > 0 and distance <= alignment_distance:
                sum_of_velocity += boid.velocity
                boid_nbr += 1

    if boid_nbr:
        # sum_of_velocity /= boid_nbr
        # var desired_velocity = sum_of_velocity.normalized() * max_speed
        return ((sum_of_velocity / float(boid_nbr)).normalized() * max_speed - velocity).clamped(max_force)
    return Vector2()

func separation(boids : Array) -> Vector2:
    var boid_nbr = 0
    var separation_force = Vector2()

    for agent_ref in boids:
        var boid = agent_ref.get_ref()
        if boid and boid != self:
            var distance : float = position.distance_to((boid as Node2D).position)
            if distance > 0 and distance <= separation_distance:
                # var diff = (position - boid.position).normalized() / distance
                # separation_force += diff
                separation_force += (position - boid.position).normalized() / distance
                boid_nbr += 1

    if boid_nbr:
        # var desired_velocity = (separation_force / agent_nbr).normalized() * max_speed
        return ((separation_force / float(boid_nbr)).normalized() * max_speed - velocity).clamped(max_force)
    else:
        return Vector2()

func flock(boids : Array) -> void:
    apply_force(cohesion(boids) * 1.0)
    apply_force(alignment(boids) * 0.8)
    apply_force(separation(boids) * 1.4)

func apply_force(force : Vector2) -> void:
    """Add a force to the current acceleration."""
    acceleration += force

func move(delta : float) -> void:
    """Add the current acceleration to the current velocity, than move the boid."""
    velocity += acceleration
    velocity = velocity.clamped(max_speed)
    rotation = velocity.angle() + PI / 2
    position += velocity * delta
    acceleration = Vector2()

func _on_VisibilityNotifier_screen_exited() -> void:
    var screen_size : Vector2 = get_viewport_rect().size

    if position.x > screen_size.x:
        position.x = 0
    elif position.x < 0:
        position.x = screen_size.x

    if position.y > screen_size.y:
        position.y = 0
    elif position.y < 0:
        position.y = screen_size.y

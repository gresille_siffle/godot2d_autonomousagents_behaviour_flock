## Godot 2D - Autonomous Agents Behaviour - Flock

> An implementation of the steering behaviour "Flocking" described by [Craig W. Reynolds] in its document [Steering Behaviors For Autonomous Characters].

[Craig W. Reynolds]: <https://www.red3d.com/cwr/>
[Steering Behaviors For Autonomous Characters]: <https://www.red3d.com/cwr/steer/gdc99/>

This program displays the flocking behaviour of several autonomous agents, represented by bird-like objects called boids.

This particular behaviour, is an emergent result of the combination of three independant group behaviours:
**alignment**, **cohesion** and **separation**. Out of the interactions between this three behaviours, you
can observe rich patterns and autonomous intelligence.

The program run on [Godot] [1] 3.1.1 game engine, and is made with the [GDScript] language.

![Simulation Screenshot ](/meta/autonomousagents_behaviour_flock.png)

## Table of Content

- [Install](#Install)
- [Usage](#Usage)
- [License](#License)

## Install

### Dependencies

Download [Godot] [2] 3.1.1 game engine. A self-contained executable of few megabytes, no installation required.

### Open the project

Download the source code and import the `project.godot` file from Godot 3.1.1.

## Usage

... todo ...

## License

[MIT](LICENSE.txt) (c) Gresille&Siffle

[1]: <https://godotengine.org/> "Godot game engine site"
[2]: <https://godotengine.org/download> "Godot game engine download page"
[GDScript]: <http://docs.godotengine.org/en/3.1/getting_started/scripting/gdscript/gdscript_basics.html>
